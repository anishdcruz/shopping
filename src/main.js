import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import App from './app'
import Index from './components/index'
import Product from './components/product'
import Login from './components/auth/login'
import Signup from './components/auth/signup'
import Reset from './components/auth/reset'
import Checkout from './components/checkout'
import About from './components/static/about'
import Contact from './components/static/contact'
import FAQ from './components/static/faq'
import Category from './components/category'
import Cart from './components/cart'

// setup sdk
YeboSDK.Config.set('store:api:version', 'v2');
YeboSDK.Config.set('store:url', 'http://kampeiro.yebo-api.com.br/api');
global.query = new YeboSDK.Products()

Vue.use(VueRouter)
Vue.use(VueResource)

const router = new VueRouter({
  history: true
})

// routes for pages
router.map({
    '/': {
        component: Index
    },
    'category/:tax': {
      component: Category
    },
    'products/:id': {
      component: Product
    },
    'cart': {
      component: Cart
    },
    'login': {
      component: Login
    },
    'signup': {
      component: Signup
    },
    'reset': {
      component: Reset
    },
    'checkout': {
      component: Checkout
    },
    'about': {
      component: About
    },
    'contact': {
      component: Contact
    },
    'faq': {
      component: FAQ
    }
})

// start the application
router.start(App, '#app')
